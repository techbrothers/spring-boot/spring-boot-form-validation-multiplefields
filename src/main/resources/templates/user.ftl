<#import "/spring.ftl" as spring>

<html>
    <head>
    <link rel="stylesheet" href="css/styles.css" type="text/css"/>
    </head>
    <body>
        <h1>Welcome to Tech Brothers - Tech Solutions</h1>
        <h3>User Form </h3>     
        <form action="user" method="POST">
            <div class="user-form">
                <div class="labels">
                    <label>User Name:</label>
                    <@spring.bind "user.name"/>
                    <input type="text" name="name"/>
                    <@spring.showErrors "<br>"/>
                </div>
                <div class="labels">
                    <label>Age:</label>
                    <@spring.bind "user.age"/>
                    <input type="text" name="age"/>
                    <@spring.showErrors "<br>"/>
                </div>
                <div class="labels">
                    <label>Email-Id:</label>
                    <@spring.bind "user.emailId"/>
                    <input type="text" name="emailId"/>
                    <@spring.showErrors "<br>"/>
                </div>
                <#if name??>
                    ${name}
                </#if>

                                
                <div>
                    <button type="submit" name="create">Create</button>
                </div>
            </div>
        </form> 
    </body>
</html>

