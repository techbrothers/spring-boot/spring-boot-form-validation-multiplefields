/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techbrothers.techsolutions.springbootfreemarker.domain;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 *
 * @author chiru
 */
public class User {
    
    @NotBlank(message = "Name should not be blank")
    private String name;
    
    private Integer age;
    
    @NotBlank(message = "Email Address hould not be blank")
    @Email(message = "Please enter valid email address")
    private String emailId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @Override
    public String toString() {
        return "User{" + "name=" + name + ", age=" + age + ", emailId=" + emailId + '}';
    }
    
      
    
}
